{ lib, pkgs, fetchFromGitHub, python3Packages }:

let
  pyzufall = pkgs.callPackage ../pyzufall { };
  bottle = pkgs.python3Packages.bottle.overrideAttrs (oldAttrs: {
    version = "0.12.25";
    src = pkgs.fetchPypi {
      pname = "bottle";
      version = "0.12.25";
      sha256 = "4anJSXCubXELP7RSYpTf64byy0qB7/OkuY3ED7Dl4CE=";
    };
  });
in
python3Packages.buildPythonPackage rec {
  pname = "satzgenerator";
  version = "3.1.2";

  src = fetchFromGitHub {
    owner = "davidak";
    repo = "satzgenerator.de";
    rev = "1edd9c6cbfca02ed46f2d538d5cf0fb29857cdfb";
    sha256 = "0by0k36shl2gn7wlljs86vd3dg1dg2r10sq406r1b8pk8f9vmk0v";
  };

  # no tests available
  doCheck = false;

  propagatedBuildInputs = with python3Packages; [ bottle gunicorn sqlalchemy_1_4 pymysql pyzufall ];

  meta = with lib; {
    description = "satzgenerator.de";
    homepage = "https://satzgenerator.de/";
    license = licenses.gpl3Plus;
    platforms = platforms.all;
    maintainers = with maintainers; [ davidak ];
  };
}
