{ config, pkgs, lib, ... }:

let
  inherit (lib) mkDefault;
in
rec {
  imports =
    [
      <home-manager/nixos>
    ];

  users.extraUsers.davidak = {
    isNormalUser = true;
    # TODO: optional if docker.enable ++ docker
    extraGroups = mkDefault [ "wheel" "networkmanager" "audio" "video" "docker" "libvirtd" "plugdev" "adbusers" ];
  };

  services.syncthing = {
    enable = mkDefault true;
    user = "davidak";
    dataDir = mkDefault config.users.users.davidak.home;
    openDefaultPorts = true;
    settings = {
      devices = { "nas" = { id = "5WUEWIO-FHLQ6BR-HJPVQBU-7ITVSF2-EB4WZ63-3UYUW6F-FNCK5EC-TWIRWQJ"; introducer = true; }; };
      folders = { "info" = { path = "/home/davidak/info"; devices = [ "nas" ]; }; };
    };
  };

  home-manager.useUserPackages = true;
  home-manager.useGlobalPkgs = true;

  home-manager.users.davidak = { pkgs, ... }: {
    #home.packages = with pkgs; [ httpie ];

    programs = {
      bash = {
        enable = true;
        historyControl = [ "ignoredups" "ignorespace" ];
      };

      ssh = {
        enable = true;
        serverAliveInterval = 60;
      };

      git = {
        enable = true;
        extraConfig = {
          init = { defaultBranch = "main"; };
          push = { default = "current"; };
          pull = { rebase = true; };
          rebase = { autoStash = true; };
        };
        lfs.enable = true;
      };
    };

    # manuals not needed
    manual.html.enable = false;
    manual.json.enable = false;
    manual.manpages.enable = false;

    home.stateVersion = "18.09";
  };
}
