{ config, ... }:

{
  # discover services on other systems
  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };
}
