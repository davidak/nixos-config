{ config, pkgs, ... }:

{
  services.boinc = {
    enable = true;
    allowRemoteGuiRpc = true;
    extraEnvPackages = with pkgs; [
      zlib # Needed for WCG OPN GPU OpenCL WU
    ];
  };
}
