{ config, pkgs, ... }:

{
  # install packages
  environment.systemPackages = with pkgs; [
    #displaycal
    rapid-photo-downloader
    darktable
  ];
}
