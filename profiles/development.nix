{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  # install packages for software development
  environment.systemPackages = with pkgs; [
    pipenv
    unstable.reuse
    gitAndTools.gh # GitHub CLI
    git-lfs

    # NixOS
    nixpkgs-review
    nixpkgs-fmt
    unstable.nix-output-monitor
  ];
}
