{ config, pkgs, lib, ... }:

let
  unstable = import <nixos-unstable> {};
  inherit (lib) optionals;
in
{
  imports =
    [
      ./common.nix
      # TODO: simplify
      #../services/avahi.nix
      ../services/avahi-server.nix
      ../services/avahi-client.nix
      ../services/fonts
      ../users/davidak/base.nix
      ../services/uwu-style.nix
      ../modules/disable-internet-at-night
    ];

  # boot splash instead of log messages
  boot.plymouth.enable = true;

  # use elementarys pantheon desktop environment
  services.xserver.enable = lib.mkDefault true;
  services.xserver.displayManager.lightdm.enable = lib.mkDefault true;
  services.xserver.desktopManager.pantheon.enable = lib.mkDefault true;
  services.xserver.desktopManager.pantheon.extraWingpanelIndicators = with pkgs; [ monitor ];

  # Enable debug output
  environment.enableDebugInfo = true;
  services.xserver.desktopManager.pantheon.debug = true;

  # disable xterm session
  services.xserver.desktopManager.xterm.enable = false;

  # enable flatpak support
  services.flatpak.enable = true;
  xdg.portal.enable = true;

  hardware.keyboard.zsa.enable = true;

  programs.ssh.startAgent = true;

  disable-internet-at-night.enable = lib.mkDefault true;

  programs.chromium = {
    enable = true;
    # See available extensions at https://chrome.google.com/webstore/category/extensions
    extensions = [
      "cbnipbdpgcncaghphljjicfgmkonflee" # Axel Springer Blocker
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
      "hjdoplcnndgiblooccencgcggcoihigg" # Terms of Service; Didn’t Read
      "mdjildafknihdffpkfmmpnpoiajfjnjd" # Consent-O-Matic
      "mnjggcdmjocbbbhaepdhchncahnbgone" # SponsorBlock for YouTube
      "gebbhagfogifgggkldgodflihgfeippi" # Return YouTube Dislike
      "kimbeggjgnmckoikpckibeoaocafcpbg" # YouTube Captions Search
      "gcbommkclmclpchllfjekcdonpmejbdp" # HTTPS Everywhere
      "oboonakemofpalcgghocfoadofidjkkk" # KeePassXC-Browser
      "fploionmjgeclbkemipmkogoaohcdbig" # Page load time
      "fhnegjjodccfaliddboelcleikbmapik" # Tab Counter
      "fpnmgdkabkmnadcjpehmlllkndpkmiak" # Wayback Machine
      "iflpdolpbldahbddinmkobondnhpikim" # Mein Grundeinkommen - CrowdBar
      "millncjiddlpgdmkklmhfadpacifaonc" # GNU Taler Wallet
    ];
    # See available options at https://chromeenterprise.google/policies/
    extraOpts = {
      "BrowserSignin" = 0;
      "BrowserAddPersonEnabled" = false;
      "SyncDisabled" = true;
      "PasswordManagerEnabled" = false;
      "AutofillAddressEnabled" = true;
      "AutofillCreditCardEnabled" = false;
      "BuiltInDnsClientEnabled" = false;
      "MetricsReportingEnabled" = true;
      "SearchSuggestEnabled" = false;
      "AlternateErrorPagesEnabled" = false;
      "UrlKeyedAnonymizedDataCollectionEnabled" = false;
      "SpellcheckEnabled" = true;
      "SpellcheckLanguage" = [
                               "de"
                               "en-US"
                             ];
      "CloudPrintSubmitEnabled" = false;
      "BlockThirdPartyCookies" = true;
      "VoiceInteractionHotwordEnabled" = false;
      "AutoplayAllowed" = false;
    };
  };

  # install packages
  environment.systemPackages = with pkgs; [
    #pulsar # insecure https://github.com/NixOS/nixpkgs/pull/262376
    apostrophe
    meld
    chromium
    google-chrome
    #firefox
    keepassxc
    libreoffice
    gimp
    todo-txt-cli
    python3Packages.xkcdpass
    yt-dlp
    remmina
    gnome-disk-utility
    monitor
    appimage-run
    usbimager
  ] ++ optionals config.services.boinc.enable [ boinc ];
}
