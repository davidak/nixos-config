{ config, pkgs, lib, ... }:

{
  imports =
  [
    ../users/davidak/personal.nix
  ];

  # install packages
  environment.systemPackages = with pkgs; [
    #virtmanager
    gnome-boxes
  ];

  # Setup container virtualization
  virtualisation.docker.enable = true;

  # Setup hypervisor virtualization
  virtualisation.libvirtd = {
    enable = true;
    qemu.package = pkgs.qemu_kvm;
  };
}
