{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in
{
  # install packages
  environment.systemPackages = with pkgs; [
    # matrix
    element-desktop
    # other messangers
    signal-desktop
    # mail
    #thunderbird
    #gnupg
    #gpa
    # rss/atom reader
    #liferea
  ];
}
