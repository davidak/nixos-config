{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
  pubkey = import ../../services/pubkey.nix;
in
{
  imports =
    [
      /etc/nixos/hardware-configuration.nix
      ../../profiles/hardware.nix
      ../../profiles/server.nix
      ../../services/avahi-server.nix
      ../../services/boinc.nix
      #../../services/drone-runner
      ../../services/backup
    ];

  boot.loader.grub.device = "/dev/sda";

  # no access time and continuous TRIM for SSD
  fileSystems."/".options = [ "noatime" "discard" ];

  networking = {
    hostName = "nas";
    domain = "lan";

    bonds.bond0 = {
      interfaces = [ "enp6s0" "enp7s0" ];
      driverOptions = {
        mode = "balance-rr";
        miimon = "100";
      };
    };

    interfaces.bond0.ipv4.addresses = [ { address = "10.0.0.4"; prefixLength = 8; } ];

    nameservers = [ "10.0.0.1" ];
    defaultGateway = "10.0.0.1";

    firewall = {
      enable = true;
      allowPing = true;
      allowedTCPPorts = [ 80 139 443 445 5000 5001 8080 8384 9000 9001 31416 19999 22000 25565 ];
      allowedTCPPortRanges = [ { from = 4000; to = 4007; } ];
      allowedUDPPorts = [ 137 138 ];
    };

    useDHCP = false;
  };

  # Monitoring
  services.netdata = {
    enable = true;
    config = {
      global = {
        "default port" = "19999";
        "bind to" = "*";
        # 7 days
        "history" = "604800";
        "error log" = "syslog";
        "debug log" = "syslog";
      };
    };
  };
  services.vnstat.enable = true;

  # SMB Shares
  services.samba = {
    enable = true;
    settings = {
      "Archiv" = {
        path = "/data/archiv";
        public = "no";
        writable = "yes";
      };
      "Backup" = {
        path = "/data/backup";
        public = "no";
        writable = "yes";
      };
      "Media" = {
        path = "/data/media";
        public = "no";
        writable = "yes";
      };
      "Upload" = {
        path = "/data/upload";
        public = "yes";
        writable = "yes";
      };
    };
  };

  # fix error in service log
  security.pam.services.samba-smbd.limits = [
    { domain = "*"; type = "soft"; item = "nofile"; value = 16384; }
    { domain = "*"; type = "hard"; item = "nofile"; value = 32768; }
  ];

  # S3 compatible Object Storage
  services.minio = {
    enable = true;
    listenAddress = ":9000";
    dataDir = [ "/data/minio" ];
    region = "eu-central-1";
  };

  services.restic.backups.nas = {
    repository = "s3:http://127.0.0.1:9000/restic/restic";
  };

  services.restic.backups.prune = {
    repository = "s3:http://127.0.0.1:9000/restic/restic";
    environmentFile = "/etc/nixos/restic-minio";
    passwordFile = "/etc/nixos/restic-password";
    paths = [ "/etc/nixos/" "/etc/passwd" "/etc/shadow" "/root" "/home" "/var/lib" "/var/www" "/var/backup/" ];
    pruneOpts = [ "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly 12" ];
    timerConfig = { OnCalendar = "Mon *-*-* 07:00"; Persistent = "true"; };
  };

  # reverse proxy for ssl encryption
  services.caddy = {
    #enable = true;
    email = "post@davidak.de";
    extraConfig = ''
      :80 {
        respond "Hello, world!"
      }
      lan.davidak.de:9001 {
        reverse_proxy 127.0.0.1:9000
      }
    '';
  };

  users.extraUsers.davidak = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [ pubkey.davidak ];
  };

  virtualisation.docker.enable = true;

  services.syncthing = {
    enable = true;
    user = "syncthing";
    guiAddress = "0.0.0.0:8384";
  };

  # IPFS Node
  services.kubo = {
    enable = true;
    autoMount = true;
    localDiscovery = true;
    enableGC = true;
    settings = {
      #Addresses.Swarm = [ "/ip4/0.0.0.0/tcp/5000" "/ip6/::/tcp/5000" ];
      #Addresses.Gateway = "/ip4/0.0.0.0/tcp/8080";
    };
  };

  # Packages
  environment.systemPackages = with pkgs; [ btrfs-progs xfsprogs vnstat samba lm_sensors yt-dlp ];

  nix.useSandbox = true;

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "18.03";
}
