# NixOS Server

for the NixOS Survey

## Services

- [LimeSurvey](https://www.limesurvey.org/en) (Apache + PHP-FPM)
- Monitoring with [netdata](https://my-netdata.io/) and [vnStat](http://humdi.net/vnstat/)
- Local backup of database
