{ config, pkgs, lib, ... }:

{
  imports =
    [
      /etc/nixos/hardware-configuration.nix
      ../../profiles/hardware.nix
      ../../profiles/common.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.enable = false;

  # no access time and continuous TRIM for SSD
  fileSystems."/".options = [ "noatime" "discard" ];

  networking = rec {
    hostName = "minimal-target";
    domain = "lan";

    interfaces = {
      enp4s0.ipv4.addresses = [
        { address = "10.0.0.51"; prefixLength = 8; }
      ];
    };

    nameservers = [ "10.0.0.1" ];
    defaultGateway = { address = "10.0.0.1"; interface = "enp4s0"; };

    firewall = {
      enable = false;
      allowPing = true;
      allowedTCPPorts = [ 80 ];
      allowedUDPPorts = [];
    };

    useDHCP = false;
  };

  # Monitoring
  services.sysstat = {
    enable = true;
    collect-frequency = "*:00/1"; # collect data every minute
  };
  environment.systemPackages = with pkgs; [ sysstat ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?
}
