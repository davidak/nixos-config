# Pinephone

[Mobile NixOS](https://mobile.nixos.org/) configuration for my [Pinephone](https://www.pine64.org/pinephone/).

## Features

- [Phosh](https://en.wikipedia.org/wiki/Phosh) desktop environment
- Apps for mobile devices

## Usage

### Build full-disk-image

First, comment out this line in the `configuration.nix`:

```
(import /root/mobile-nixos/lib/configuration.nix { device = "pine64-pinephone"; })
```

Setup your host system for cross-compiling for arm by adding this to your config:

```
boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
```

Clone my fork of mobile-nixos which combines PR [#467](https://github.com/NixOS/mobile-nixos/pull/467) and [#468](https://github.com/NixOS/mobile-nixos/pull/468).

```
git clone https://github.com/davidak/mobile-nixos.git
git checkout phosh-and-5.17
```

Clone nixpkgs (if you haven't already) and checkout this commit known to work well:

```
git clone https://github.com/NixOS/nixpkgs.git
git checkout 17c252aab1772d36acd6d3f57f6512f25b6f9e9c
```

Then you can build the full-disk-image using:

```
export NIX_PATH="mobile-nixos=~/code/mobile-nixos"
NIXPKGS_ALLOW_UNFREE=1 nix-build ~/code/nixos-config/machines/pinephone --arg pkgs 'import ~/code/nixpkgs {}' --system aarch64-linux --argstr device pine64-pinephone -A outputs.default
```

(change paths to the repos on your system)

Copy it to a microSD card using:

```
sudo dd if=/nix/store/gs6kzw3vhpdwkrmzk6wxd68fhap6hsdr-pine64-pinephone_full-disk-image.img of=/dev/sdb bs=8M oflag=sync,direct status=progress
```

Put it into the Pinephone and boot it.

## Resize partition

The system will complain that the disk is full. That is because the partition and filesystem is only as big as the files.

To resize the partition and filesystem, use:

```
cfdisk /dev/mmcblk0
```

Select "Resize" and select the last partition to use all the free space.

Then run:

```
resize2fs /dev/disk/by-label/NIXOS_SYSTEM
```

## Change configuration

Comment in the device line again.

Copy this repo to the device using rsync as described in the [README](../../README.md) and create a symlink for the config.

Now you can use change the config and rebuild the system.

Since later versions of nixpkgs have an issue ([#173832](https://github.com/NixOS/nixpkgs/issues/173832)), use this specific version:

```
nixos-rebuild switch -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/17c252aab1772d36acd6d3f57f6512f25b6f9e9c.tar.gz
```

## Install to internal eMMC storage

I have no idea.

https://github.com/NixOS/mobile-nixos/issues/471

## Links

Here are more resources:

- https://mobile.nixos.org/devices/pine64-pinephone.html
- https://nixos.wiki/wiki/PinePhone
- https://git.sr.ht/~tomf/notes/tree/master/item/pinephone-nixos-getting-started.md
