{ config, pkgs ? (import ./pkgs.nix {}), ... }:

{
  imports =
    [
      (import /root/mobile-nixos/lib/configuration.nix { device = "pine64-pinephone"; })
      ../../services/ssh.nix
      ../../services/nix.nix
      ../../services/localization.nix
    ];

  nixpkgs.localSystem = { system = "aarch64-linux"; };

  networking = {
    hostName = "pinephone";

    firewall.enable = true;
  };

  users.users.davidak = {
    isNormalUser = true;
    password = "1234";
    extraGroups = [
      "dialout"
      "feedbackd"
      "networkmanager"
      "video"
      "wheel"
    ];
  };

  services.xserver.desktopManager.phosh = {
    enable = true;
    user = "davidak";
    group = "users";
  };

  programs.calls.enable = true;
  hardware.sensor.iio.enable = true;

  environment.systemPackages = [
    pkgs.chatty
    pkgs.kgx
    pkgs.megapixels
  ];

  # copy the system configuration into nix-store
  # broken https://github.com/NixOS/mobile-nixos/issues/338#issuecomment-1133476440
  #system.copySystemConfiguration = true;

  # compatible NixOS release
  system.stateVersion = "22.05";
}
